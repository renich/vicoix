=============================================================
Virtualización y Contenedores @ Ixtlahuacán de los Membrillos
=============================================================
------------------------------------------------------------------------
Proyecto de laboratorio para prácticas con virtualización y contenedores
------------------------------------------------------------------------

Descripción
===========
Este proyecto es para describir, planear y realizar un proyecto de laboratorio para hacer prácticas en ambientes de virtualización,
contenedores y tecnologías relacionadas; haciéndolo disponible para la comunidad internacional, documentando los procesos y
colaborando con el mundo para documentar, descifrar y en.. contents:: Table of Contentstender mejor estas tecnologías en el idioma
español.

Instrucciones
=============

Leer en línea
-------------
Para leer el proyecto, ve a: `proyecto.rst <proyecto.rst>`_.

PDF
---
Si prefieres leerlo en formato PDF, debes:

* Instalar `GNU make` y `rst2pdf`.
* Clonar el repositorio.
* Correr: ``make pdf``.

Una vez hecho eso, el PDF estará disponible en `pdf/proyecto.pdf`.

