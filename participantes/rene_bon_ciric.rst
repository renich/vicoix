René Bon Ćirić
==============

Profesión:
    Consultor FOSS, DevOps, Arquitecto y Compositor Creative Commons

Fecha de nacimiento:
    7 de Febrero, 1981

Contacto:
    * móvil: +52 (33) 3576-5013
    * correo: renich@evalinux.com

Sitio(s) Web:
    * https://evalinux.com/
    * https://introbella.com/
    * https://woralelandia.com/

Bio:
    Me dedico a dar consultoría, arquitectar, implementar y mantener sistemas basados en FOSS; tales como:

    * Nubes basadas en OpenStack o KVM/Qemu vainilla.
    * Orquestación y clústeres de contenedores con Kubernetes.
    * Clústeres de bases de datos (Galera/MariaDB y PostgreSQL).
    * Servidores de aplicación (Nginx, Caddy y Apache httpd).
    * Servidores de utilería (DNS, DHCP, NTP, etc).
    * Firewalls y ruteadores (nftables e iptables en Linux).
    * etc.

    También, soy compositor, productor, arreglista e ingeniero de sonido de música Creative Commons.

