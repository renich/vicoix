Participantes
=============
Aquí están los participantes de este proyecto. Si estás participando y quieres aparecer aquí, copia el archivo `rene_bon_ciric.rst`
y agrega tus datos.

Formato
=======
Debes nombrar el archivo igual que tu nombre; en minúsuclas y sin espacios; usando guión bajo `"_"` como separador.

