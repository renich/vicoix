===============================================================================
Virtualización, Contenedores y Machine Learning @ Ixtlahuacán de los Membrillos
===============================================================================
--------------------------------------------------------------------------------------------------
Proyecto de centro de investigación y laboratorio para prácticas con virtualización y contenedores
--------------------------------------------------------------------------------------------------

.. meta::
    :Date: 2024-10-06
    :Version: 2
    :Authors: - René Bon Ćirić <renich@evalinux.com>
    :Copyright: CC-BY-SA 4 o >

.. raw:: pdf

    FrameBreak

.. contents:: Tabla de contenido

.. raw:: pdf

    FrameBreak

Descripción
===========
Este proyecto es para describir, planear y realizar un proyecto de centro de investigación y laboratorio para hacer prácticas en
ambientes de virtualización, contenedores, machine learning y tecnologías relacionadas; haciéndolo disponible para la comunidad
internacional, documentando los procesos y colaborando con el mundo para documentar, descifrar y entender mejor estas tecnologías en
el idioma español.

Problemática
============
Las tecnologías de información al rededor de la virtualización, los contenedores y machine learning son ubicuas. Dicho ésto, aún
siendo públicos la información, los manuales y los canales de comunicación, es bastante pronunciada la curva de aprendizaje. Es
bastante difícil hacer sentido de la información disponible dado que, casi en su totalidad, está disponible en Inglés.

Dada, ya, esta dificultad, también es bastante difícil adquirir el equipo necesario para hacer experimentos, practicar o aprender a
usar, instalar, configurar e integrar este tipo de tecnologías. El equipo es muy costoso, difícil de manejar y está poco disponible.

Ésto causa el que sea bastante difícil introducirse en este ambiente y aprender a usar esta teconología.

Por otro lado, incluso en el ámbito profesional; por lo menos en México, pocas veces hay recursos soficientes para poner un
ambiente adecuado de pruebas; para poder probar las actualizaciones, configuraciones y movimientos antes de hacer los cambios
en un ambiente de producción, lo cual no le permite, a los profesionales, probar las cosas antes de hacerlas; causando,
frecuentemente, problemas.

Solución
========
Crear un centro de investigación y laboratorio con lo necesario para poder hacer pruebas, prácticas, experimentos, sesiones de
aprendizaje y cursos; experimentándo y, a su vez, documentando el proceso para su publicación y hacerlo disponible a la comunidad;
incluyendo:

* Personas interesadas.
* Profesionales.
* Escuelas.
* Grupos de interés común.
* Comunidad de DevOps, SysAdmins, desarrolladores e integradores.
* Empresas.
* Asociaciones civiles.
* Sociedades civiles.
* etc.

A su vez, aprovechar el uso para documentar y publicar las mejores configuraciones, las mejores prácticas, documentar optimizaciones
y diversas maneras de configuración; valiéndonos de:

* Wiki.
* Video.
* Infográficos.

Componentes
-----------
El laboratorio deberá contener lo necesario para hospedar a los servidores, tarjetas gráficas, switches, routers y demás equipo
especializado.

Racks:
    Los Racks son los estantes en donde se acomodarán los servidores.

Router/Firewall:
    Es el equipo que regulará el tráfico entrante y saliente hacia y desde los servidores.

Switches:
    Son los que proveen el servicio de red hacia los servidores.

Servidores para virtualización:
    Son los servidores necesarios para correr el servicio de virtualización. Éstos son multi-propósito ya que pueden utilizarse para
    usarlos como nodos miembros de un clúster para uso de contenedores.

Servidores para almacenamiento distribuido:
    Aquí se instalará el servicio de almacenamiento distribuido; el cual puede ser usado por los servidores de virtualización como capa
    de almacenamiento. Es donde se guardan los datos de las implementaciones realizadas.

Servidores de utilería:
    Son para proveer los servicios de utilería (DNS, DHCP, NTP, etc); los cuales son escenciales para todos los servicios provistos por
    el laboratorio.

Servidor de respaldos:
    Es el servidor que se encargará de hacer respaldos de manera automatizada; en caso de ser requerido por los proyectos de
    implementación que se lleven a cabo.

Fuente de alimentación eléctrica ininterrumpida:
    Mantiene la electricidad acondicionada y modulada para proteger a los equipos de los alti-bajos de electricidad que pudiera sufrir
    el centro de datos.

Aires acondicionados:
    Mantienen la temperatura de operación óptima para los equipos del laboratorio.

Tarjetas gráficas:
    Se utilizan para hacer el procesamiento necesario para el machine learning.


Valor agregado
==============

Educación con impacto mundial
-----------------------------
Al no tener en donde practicar, las personas inteteresadas en estos temas, pierden la oportunidad de incursionar en esta rama. Al
tener un laboratorio para hacer prácticas, aunado a la documentación en castellano y las recetas pre-fabricadas que hay, le
permitirán al usuario el poder practicar lo mencionado, ayudando grandemente en su educación.

A su vez, al ayudar a generar a más expertos en la materia, estos expertos colaborarán con el laboratorio para enriquecer la
documentación, los procedimientos y la información provista; creando un círculo virtuoso que beneficiará a toda la comunidad.

El impacto, sin duda, será mundial.

Ambiente de pruebas para profesionales
--------------------------------------
Muchos de los proyectos de machine learning, nubes o clústeres de orquestación de contenedores no consideran el costo de desplegar
un ambiente de pruebas idéntico al de producción. A veces porque es muy costoso, o tras veces por falta de planeación.

Lo siguiente mejor es tener un ambiente de pruebas. El laboratorio puede proveer ese ambiente a empresas y profesionales para que
puedan hacer uso del mismo y llevar a cabo pruebas de:

* actualizaciones.
* adicción de funcionalidad.
* cambios de arquitectura.
* cambios de configuración.
* cambios de infraestructura.
* despliegue.
* pruebas y corroboración de automatización.
* rollbacks.
* etc.

Ésto prevendrá que, como seguido pasa, se hagan estas pruebas en el ambiente de producción; arriesgando la funcionalidad y
permanencia de los servicios ahí ubicados.

Documentación comprensiva en castellano
---------------------------------------
El documentar todas las implementaciones hechas en el laboratorio, proveeremos de un recurso muy valiioso a la comunidad para poder
replicar lo hecho ahí. Ésto habilitará a más personas a poder hacer implementaciones de nube, de clústeres de kubernetes; en
cualquiera de sus variaciones, OpenStack y demás plataformas abiertas.

Al haber más personas que sepan hacer este tipo de cosas, se hará más amplia la oferta de personal especializado en un mundo en
donde estos ambientes están proliferando; dándole más oportunidades a los participantes y reduciendo el costo de educación e
inducción a las empresas y organizaciones que los requieren.

Reproducibilidad
----------------
Desde el principio, se trabajará en hacer disponible, en Internet, todo lo necesario para poder hacer reproducible el laboratorio.
De esta manera, permitimos a la comunidad el participar en el diseño del mismo, así como de su optimización.

Se proveerán:

* Modelos de routers, firewalls, switches y servidores utilizados; así como una lista de alternativas.
* Diagramas de red.
* Plano de arquitectura.
* Configuraciones de routers, firewalls, switches y servidores para facilitar su implementación.
* Diagramas de instalaciónes eléctricas, de enfriamiento, etc.
* Consejos prácticos.

Todo se hará en formatos abiertos para hacer fácil la participación y la colaboración.

.. raw:: pdf

    FrameBreak

Presupuesto
============
El presupuesto está en `odf/presupuesto.fods <https://gitlab.com/renich/vicoix/-/raw/master/odf/presupuesto.fods?inline=false>`_; el
cual es un documento en formato ODF y puede ser descargado y visto usando LibreOffice.

He aquí una representación en texto pero debe considerarse el archivo antes mencionado como la versión definitiva.

Equipo
------

=========== ================================================        ===============     ===========     =============
Presupuesto
---------------------------------------------------------------------------------------------------------------------
Cantidad    Descripción                                             Precio Unitario     Totales         Observaciones
=========== ================================================        ===============     ===========     =============
6           Servidores para visualización                           $275,000.00         $1,650,000
6           Servidores para almacenamiento distribuido              $300,000.00         $1,800,000
3           Servidor de artillería                                  $275,000.00         $825,000
1           Servidor para respaldos                                 $400,000.00         $400,000
3           Switches con 24 puertos a 25 Gbps                       $75,000.00          $225,000
1           Router/firewall                                         $50,000.00          $50,000
3           Racks                                                   $30,000.00          $90,000
1           Fuente de alimentación eléctrica ininterrumpida         $120,000.00         $120,000
2           Acondicionadores de aire                                $30,000.00          $60,000
2           Tarjetas gráficas Nvidia H100                           $800,000.00         $1,600,000
2           Tarjetas gráficas AMD Instrinct MI300                   $400,000.00         $800,000
|
|
            Total                                                                       $7,620,000
=========== ================================================        ===============     ===========     =============

.. raw:: pdf

    FrameBreak

Servicios
---------

=========== ================================================        ===============     ===========     =============
Presupuesto
---------------------------------------------------------------------------------------------------------------------
Meses       Descripción                                             Costo menusal       Totales         Observaciones
=========== ================================================        ===============     ===========     =============
12          Conexión a Internet (400 Mbps)                          $2,000              $24,000
12          Servicio de alarma y monitoreo                          $5,000              $60,000
12          Servicio de electricidad                                $8,000              $96,000
|
|
            Total                                                                       $180,000        anuales
=========== ================================================        ===============     ===========     =============

.. raw:: pdf

    FrameBreak

Personal
--------

=========== ================================================        ===============     ===========     =============
Presupuesto
---------------------------------------------------------------------------------------------------------------------
Meses       Descripción                                             Salario mensual     Totales         Observaciones
=========== ================================================        ===============     ===========     =============
12          Administrador de sistemas                               $50,000             $60,000
12          Administración y contabilidad                           $5,000              $60,000
12          Mantenimiento y limpieza                                $5,000              $60,000
12          Mantenimiento y limpieza                                $8,000              $96,000
12          Vigilancia                                              $8,000              $96,000
|
|
            Total                                                                       $816,000        anuales
=========== ================================================        ===============     ===========     =============

.. raw:: pdf

    FrameBreak

Infraestructura
---------------

=========== ================================================        ===============     ===========     =============
Presupuesto
---------------------------------------------------------------------------------------------------------------------
Meses       Descripción                                             Salario mensual     Totales         Observaciones
=========== ================================================        ===============     ===========     =============
12          Renta                                                   $10,000             $120,000
|
|
            Total                                                                       $120,000        anuales
=========== ================================================        ===============     ===========     =============

.. raw:: pdf

    FrameBreak

Licencia
========
CC-BY 4.0 o >

Se puede consultar en: https://creativecommons.org/licenses/by/4.0/


Contacto
========
René Bon Ćirić <renich@evalinux.com>

Sitio web
---------
https://evalinux.com/

Dirección
---------
::

    José Antonio Torres #72b
    Ojo de Agua, 45850,
    Ixtlahuacán de los Membrillos, Jalisco, México

Comunicación
------------
Teléfono:
    +52 33 3576-5013

Correo:
    * renich@evalinux.com
    * renich@woralelandia.com
