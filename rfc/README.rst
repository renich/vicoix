Request For Changes
===================
En este proyecto, un RFC o, en Español, Petición de Cambio, es un documento que sugiere cambios al documento oficial.

Por ejemplo, si consideras que la problemática está mal delimitada, aquí puedes indicar el problema, proponer un cambio y
justificarlo.

Acompáñalo de un talón o "issue", por favor, y haces una petición de inclusión o "pull request" para resolver ese talón.

