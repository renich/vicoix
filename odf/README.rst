Open Document Format
====================
Aquí van archivos en formato ODF.

Éstos deben aportar información al proyecto de forma práctica.

Formato
=======
Los documentos deben usar el formato Flat XML ODF para que sea fácil para git seguir los cambios; a diferencia del formato ODF; el
cual es un formato comprimido (binario).
