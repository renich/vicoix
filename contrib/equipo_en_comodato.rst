Equipo en comodato
==================

.. meta::
    :Date: 2022-10-01
    :Version: 1
    :Authors: - René Bon Ćirić <renich@evalinux.com>
    :Copyright: CC-BY-SA 4 or >

Propuesta
---------
Se puede hacer el esfuerzo de procurar equipo en comodato. Ésto significa que, una empresa, pudiera "prestarnos" el equipo ya sea
indefinidamente o por un tiempo limitado.

Es probable que haya equipo que necesitemos ahí permanentemente. Por ejemplo:

* racks.
* servidores de instalación/utilería.
* fuentes de poder.
* acondicionadores de corriente y clima.

Pero, los servidores del clúster, definitivamente pudieran ser intercambiables.

Pro
---
* Reduciríamos el costo del proyecto mucho.
* La variación de equipo pudiera ser muy constructiva para las personas que quieren practicar despliegues de este tipo de nubes.
* Potencialmente, sería más fácil mantener el equipo al día y probar equipo nuevo; acutalizando o adendando a las guías de
  despliegue.
* Abre la puerta a la posibilidad de certificar equipo específico para despliegues específicos.

Contra
------
* Se hace incierta la disponibilidad de equipo en el laboratorio.
* En caso de daño o pérdida del equipo, pudiera llegar a considerarse responsable al laboratorio por el mismo.
* Aumenta mucho la corresponsabilidad de las personas que lo usen.
