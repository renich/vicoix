Contribuciones
==============
El proyecto principal es el proyecto oficial. En esta sección, puedes agregar caminos alternos para lograr el mismo propósito.

Por ejemplo, si crees que la arquitectura debe ser diferente, puedes darle nombre a tu propuesta y agregarla aquí como alternativa a
lo propuesto en el proyecto oficial.

Un ejemplo de ésto sería utilizar servidores usados en vez de comprarlos o pedirlos donados; o en comodato. Pupdieras hacer una guía
para comprar los servidores; detallando como.

Instrucciones
-------------
Para hacer una propuesta, solo tines que:

* Clonar el repositorio.
* Copiar `plantilla.rst` a `título_de_tu_propuesta.rst`; en ``contrib/``.
* Elaborarla.
* Revísala.
* Haz tu aportación mediante una petición de inclusión o  "pull request".

Sugerencias
-----------
* Asegúrate de agregar bien tus metadatos.
* Cuida mucho tu ortografía.
* No uses anglicismos innecesarios. Habemos varios que los detestamos.
* Redacta bien tus propuestas.

