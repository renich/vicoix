Equipo usado
============

.. meta::
    :Date: 2022-10-01
    :Version: 1
    :Authors: - René Bon Ćirić <renich@evalinux.com>
    :Copyright: CC-BY-SA 4 or >

Propuesta
---------
Pudiéramos comprar servidores usados en ebay.com por ejemplo. Al parecer, hay varios servidores que cuestan una fracción del valor
original. Por ejemplo: `Búsqueda de PowerEdge @ eBay.com`_

Es fácil, al parecer, encontrar servidores muy baratos; aunque 1 o 2 generaciones anteriores.

.. _`Búsqueda de PowerEdge @ eBay.com`: https://www.ebay.com/sch/i.html?_dmd=1&RAM%2520Size=64%2520GB&_nkw=dell+poweredge&_sacat=11211&Model=Dell%2520PowerEdge%2520R710%7CDell%2520PowerEdge%2520R730%7CDell%2520PowerEdge%2520R820%7CDell%2520PowerEdge%2520R910%7CDell%2520PowerEdge%2520T710%7CDell%2520Poweredge%2520R715%7CDell%2520Poweredge%2520R720Xd%7CDell%2520Poweredge%2520R810%7CDell%2520Poweredge%2520R900%7CPowerEdge%2520R730xd%7CPoweredge%2520R630&_dcat=11211&_sop=15&_trkparms=pageci%3A1b216535-41b6-11ed-9600-6274080e87de%7Cparentrq%3A94c9b7a61830a0ac23bf0addfffb7e84%7Ciid%3A1

Pro
---
* Una solución con este tipo de hardware pudiera abaratar los costos muchísimo.
* La experiencia, de escoger bien el hardware, pudiera ser casi igual a la de comprar hardware nuevo. Incluso, sería más cercana a
  la realidad ya que, en la industria, nos toparemos, muy seguidos, con equipos que no son de última generación.

Contra
------
* Pudiera ser que compremos equipo fallido y, seguramente, no cuentan con garantía alguna.
* Va a ser difícil obtener factura por la compra de estos equipos.
* Es probable que terminemos con un clúster con hardware mixto. No estoy seguro si ésto es una contra o un pro.

